
public class CarWithEngineComposition {

	private Engine engine;
	int numDoors;
	
	public CarWithEngineComposition() {
		engine = new Engine("4 cylinder");
		numDoors = 4;
	}
	
	public String toString() {
		return "Car details: \n\t" + engine + "\n\tNumber of Doors: " + numDoors;
	}
	
	
}
