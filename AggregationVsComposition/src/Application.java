
public class Application {

	public static void main(String[] args) {
		
		CarWithEngineComposition corolla = new CarWithEngineComposition();
		System.out.println(corolla);
		
		Engine engine = new Engine("6 cylinder");
		CarWithEngineAggregation accord = new CarWithEngineAggregation(engine);
		System.out.println(accord);

	}

}
