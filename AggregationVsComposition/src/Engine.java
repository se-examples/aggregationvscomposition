
public class Engine {

	private String description;
	
	public Engine(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "Engine description: " + description;
	}
	
}
