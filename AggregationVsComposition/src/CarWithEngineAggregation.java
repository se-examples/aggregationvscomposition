
public class CarWithEngineAggregation {

	private Engine engine;
	int numDoors;
	
	public CarWithEngineAggregation(Engine engine) {
		this.engine = engine;
		numDoors = 4;
	}
	
	public String toString() {
		return "Car details: \n\t" + engine + "\n\tNumber of Doors: " + numDoors;
	}
	
	
}
